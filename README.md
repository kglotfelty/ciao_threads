# CIAO Threads

This is a collection of the notebooks that run the 
[CIAO threads](http://cxc.cfa.harvard.edu/ciao/threads)

These notebooks require the `bash kernel` be installed 
(and working, which seems to be a problem for some people
these days.)

The text of the CIAO threads was intentionally omitted -- hopefully
the commands and the minimal header lines are enough to be useful
to sync up the notebooks and the threads.


